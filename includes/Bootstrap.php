<?php

namespace Sachyya\AdminStatusMessage;

use Sachyya\AdminStatusMessage\Admin\Setting;
use Sachyya\AdminStatusMessage\Admin\Notice;
use Sachyya\AdminStatusMessage\Admin\DashboardWidget;

/**
 * Final class to boot the plugin
 */
final class Bootstrap {

	public static ?Bootstrap $_instance = null;

	/**
	 * Gets the instance.
	 *
	 * @return     Bootstrap|null  The instance.
	 */
	public static function get_instance(): ?Bootstrap {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}

		return self:: $_instance;
	}

	/**
	 * Bootstrap constructor.
	 */
	public function __construct() {
		$this->autoload();
		register_activation_hook( ASM_FILE_PATH, [ $this, 'plugin_activated' ] );
		register_deactivation_hook( ASM_FILE_PATH, [ $this, 'plugin_deactivated' ] );
		add_action( 'init', [ $this, 'load_textdomain' ] );
		add_action( 'plugins_loaded', [ $this, 'init_plugin' ] );
	}

	/**
	 * Include autoloader created by Composer for PSR-4
	 */
	public function autoload() {
		require_once ASM_ROOT_DIR_PATH . '/vendor/autoload.php';
	}

	public function plugin_activated() {
		update_option( 'asm_plugin_activate', 'activated' );
	}

	public function plugin_deactivated() {
		delete_option( 'asm_plugin_activate' );
	}

	/**
	 * Loads textdomain.
	 */
	public function load_textdomain() {
		load_plugin_textdomain( 'admin-status-message', false, dirname( plugin_basename( ASM_FILE_PATH ) ) . '/languages' );
	}


	/**
	 * Initializes the plugin.
	 */
	public function init_plugin() {
		Setting::get_instance();

		$message = Setting::get_message();
		if(  'notice' === $message['display_type'] ) {
			Notice::get_instance();
		} else {
			DashboardWidget::get_instance();
		}
	}
}

Bootstrap::get_instance();