<?php

namespace Sachyya\AdminStatusMessage\Admin;

use Sachyya\AdminStatusMessage\Admin\Setting;

/**
 * Class to display the message as dashboard widgets
 */
class DashboardWidget {
	public static ?DashboardWidget $instance = null;


	/**
	 * Gets the instance.
	 *
	 * @return     DashboardWidget|null  The instance.
	 */
	public static function get_instance(): ?DashboardWidget {
		if ( is_null( self::$instance ) ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * DashboardWidget constructor
	 */
	public function __construct() {
		if( is_multisite() ) { // have to check is_multisite since is_network_admin does not check for multisite
			if( is_network_admin() ) { // run only on network admin dashboard
				add_action('wp_network_dashboard_setup', [ $this, 'dashboard_widget']  );
			} else {
				add_action('wp_dashboard_setup', [ $this, 'dashboard_widget']  );
			}
		} else {
			add_action('wp_dashboard_setup', [ $this, 'dashboard_widget']  );
		}
	}

	/**
	 * Add widget and pull the widget to the top.
	 */
	public function dashboard_widget() {
		wp_add_dashboard_widget( 'asm_widget', apply_filters( 'asm_widget_title', __( 'Admin Status Message', 'admin-status-message' ) ), [ $this, 'dashboard_widget_cb' ] );

		//Pull the added widget to the top
		
	    // Globalize the metaboxes array, this holds all the widgets for wp-admin.
	    global $wp_meta_boxes;
	    
	    if( is_multisite() ) { // have to check is_multisite since is_network_admin does not check for multisite
			if( is_network_admin() ) { // run only on network admin dashboard
				 $dashboard_index = 'dashboard-network';
			} else {
				 $dashboard_index = 'dashboard';
			}
		} else {
			$dashboard_index = 'dashboard';
		}

	    // Get the regular dashboard widgets array 
	    // (which already has our new widget but appended at the end).
	    $default_dashboard = $wp_meta_boxes[ $dashboard_index ]['normal']['core'];
	     
	    // Backup and delete our new dashboard widget from the end of the array.
	    $asm_widget_backup = [ 'asm_widget' => $default_dashboard['asm_widget'] ];
	    unset( $default_dashboard['asm_widget'] );
	  
	    // Merge the two arrays together so our widget is at the beginning.
	    $sorted_dashboard = array_merge( $asm_widget_backup, $default_dashboard );
	  
	    // Save the sorted array back into the original metaboxes. 
	    $wp_meta_boxes[ $dashboard_index ]['normal']['core'] = $sorted_dashboard;
	}
		 
	/**
	 * Callback function for the content of the widget
	 */
	public function dashboard_widget_cb() {
		$message = Setting::get_message();
		echo esc_html( $message['message'] );
	}
}