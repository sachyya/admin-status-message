<?php

namespace Sachyya\AdminStatusMessage\Admin;


/**
 * Class to define all the settings and some helper functions
 */
class Setting {
	public static ?Setting $instance = null;


	/**
	 * Gets the instance.
	 *
	 * @return     Setting|null  The instance.
	 */
	public static function get_instance(): ?Setting {
		if ( is_null( self::$instance ) ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Setting constructor
	 */
	public function __construct() {
		add_action( 'admin_menu', [ $this, 'add_submenu_page' ] );
	}

	/**
	 * Adds a submenu page to tools.php and register settings to the page.
	 */
	public function add_submenu_page() {
		add_submenu_page( 'tools.php',
			__( 'Admin Status Message', 'admin-status-message' ),
			__( 'Admin Status Message', 'admin-status-message' ),
			'manage_options',
			'admin-status-message',
			[ $this, 'setting_page_cb' ] );

		// Added on admin_menu hook so that settings are registed only when subpage is being added.
		add_action( 'admin_init', [ $this, 'register_settings' ] );

		// Enqueue scripts on admin
		add_action( 'admin_enqueue_scripts', [ $this, 'load_scripts' ] );
	}

	/**
	 * Callback function for submenu page to dispaly the settings
	 */
	public function setting_page_cb() {
		// Check user capabilities
	    if ( ! current_user_can( 'manage_options' ) ) {
	        return;
	    }
	 
	    // check if the user have submitted the settings WordPress will add the "settings-updated" $_GET parameter to the url
	    if ( isset( $_GET['settings-updated'] ) ) {
	        // add settings saved message with the class of "updated"
	        add_settings_error( 'asm_messages', 'asm_message', __( 'Settings Saved', 'admin-status-message' ), 'updated' );
	    }
	 
	    // show error/update messages
	    settings_errors( 'asm_messages' );
	    ?>
	    <div class="wrap">
	        <h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
	        <form action="options.php" method="post">
	            <?php
	            // Output security fields like nonce for the registered setting
	            settings_fields( 'asm_settings' );
	            
	            // Output setting sections and their fields
	            do_settings_sections( 'admin-status-message' );

	            // Output save settings button
	            submit_button( __( 'Save Message', 'admin-status-message' ) );
	            ?>
	        </form>
	    </div>
	    <?php
	}


	/**
	 * Register custom settings.
	 */
	function register_settings() {
		$setting_args = [
            'sanitize_callback' => [ $this, 'sanitize_fields' ],
        ];

	    // Register a new setting for page.
	    register_setting( 'asm_settings', 'asm_message', $setting_args ); // value is auto sanitized if no $args['sanitize_callback'] is passed
	 
	    // Register a new section in the page.
	    add_settings_section(
	        'asm_text_section',
	        __( 'Message Settings', 'admin-status-message' ), 
	        '',
	        'admin-status-message'
	    );
	 
	    // Register a text field in the "asm_text_section" section.
	    add_settings_field(
	        'asm_text',
	        __( 'Message', 'admin-status-message' ),
	        [ $this, 'message_field_cb' ],
	        'admin-status-message',
	        'asm_text_section',
	        [
	            'label_for'	=> 'asm_message_field',
	        ]
	    );

	    // Register a message type field in the "asm_text_section" section.
	    add_settings_field(
	        'asm_message_display_type',
	        __( 'Display as', 'admin-status-message' ),
	        [ $this, 'message_display_type_field_cb' ],
	        'admin-status-message',
	        'asm_text_section',
	        [
	            'label_for'	=> 'asm_message_display_type',
	        ]
	    );

	    $options = get_option( 'asm_message' );
	    $custom_data['label_for'] = 'asm_message_type';

	    if( isset( $options['asm_message_display_type'] ) && $options['asm_message_display_type'] !== 'notice' ){
	    	$custom_data['class'] = 'hidden';
	    }
	    // Register a message type field in the "asm_text_section" section.
	    add_settings_field(
	        'asm_message_type',
	        __( 'Message type', 'admin-status-message' ),
	        [ $this, 'message_type_field_cb' ],
	        'admin-status-message',
	        'asm_text_section',
	        $custom_data
	    );

	    // Add setting only to the main site and available only to super admins.
	    if( is_multisite() && is_main_site() && is_super_admin() ) {
		    add_settings_field(
		        'asm_enable_network_wide_message',
		        __( 'Enable network wide message', 'admin-status-message' ),
		        [ $this, 'enable_network_wide_message_cb' ],
		        'admin-status-message',
		        'asm_text_section',
		        [
		            'label_for'	=> 'asm_enable_network_wide_message',
		        ]
		    );

		    $network_wide_custom_data['label_for'] = 'asm_network_wide_message';
		    if( isset( $options['asm_enable_network_wide_message'] ) && ! $options['asm_enable_network_wide_message'] ){
		    	$network_wide_custom_data['class'] = 'hidden';
		    }

		    // Register a text field in the "asm_text_section" section.
		    add_settings_field(
		        'asm_network_wide_message',
		        __( 'Network wide status message', 'admin-status-message' ),
		        [ $this, 'network_wide_message_cb' ],
		        'admin-status-message',
		        'asm_text_section',
		        $network_wide_custom_data
		    );

		    $network_wide_type_custom_data['label_for'] = 'asm_network_wide_message_type';
		    if( isset( $options['asm_enable_network_wide_message'] ) && ! $options['asm_enable_network_wide_message'] ){
		    	$network_wide_type_custom_data['class'] = 'hidden';
		    }
		    // Register a message type field in the "asm_text_section" section.
		    add_settings_field(
		        'asm_network_wide_message_type',
		        __( 'Network wide status message type', 'admin-status-message' ),
		        [ $this, 'message_type_field_cb' ],
		        'admin-status-message',
		        'asm_text_section',
		        $network_wide_type_custom_data
		    );
		}
	}

	
	/**
	 * Output the text field for message
	 *
	 * @param      array  $args 
	 */
	function message_field_cb( $args ) {
	    // Get the value of the setting we've registered with register_setting()
	    $options = get_option( 'asm_message' );
	    ?>
	    <input 
	    	type="text"
	    	id="<?php echo esc_attr( $args['label_for'] ); ?>"
	        name="asm_message[<?php echo esc_attr( $args['label_for'] ); ?>]"
	        value="<?php echo isset( $options[ $args['label_for'] ] ) ? esc_attr( $options[ $args['label_for'] ] ) : ''; ?>"
	    />
	    <?php
	}

	/**
	 * Message type field callback function.
	 *
	 * @param array $args
	 */
	function message_display_type_field_cb( $args ) {
	    // Get the value of the setting we've registered with register_setting()
	    $options = get_option( 'asm_message' );
        
        // Provided filter so extra options can be added to the options.
        $message_type_arr = ['notice', 'Dashboard Widget'];
        ?>
	    <select
            id="<?php echo esc_attr( $args['label_for'] ); ?>"
            name="asm_message[<?php echo esc_attr( $args['label_for'] ); ?>]">
        	
        	<?php foreach ( $message_type_arr as $message_type ) { ?>

	            <option 
	            	value="<?php echo esc_attr( $message_type ); ?>" 
	            	<?php echo isset( $options[ $args['label_for'] ] ) ? ( selected( $options[ $args['label_for'] ], esc_attr( $message_type ), false ) ) : ''; ?>>
	                <?php echo ucfirst( esc_attr( $message_type ) ); ?>
	            </option>

        	<?php } ?>
        </select>
	    <?php
	}

	/**
	 * Message type field callback function.
	 *
	 * @param array $args
	 */
	function message_type_field_cb( $args ) {
	    // Get the value of the setting we've registered with register_setting()
	    $options = get_option( 'asm_message' );
        
        // Provided filter so extra options can be added to the options.
        $message_type_arr = apply_filters( 'message_type_arr', ['success', 'info', 'warning', 'error'] );
        ?>
	    <select
            id="<?php echo esc_attr( $args['label_for'] ); ?>"
            name="asm_message[<?php echo esc_attr( $args['label_for'] ); ?>]">
        	
        	<?php foreach ( $message_type_arr as $message_type ) { ?>

	            <option 
	            	value="<?php echo esc_attr( $message_type ); ?>" 
	            	<?php echo isset( $options[ $args['label_for'] ] ) ? ( selected( $options[ $args['label_for'] ], esc_attr( $message_type ), false ) ) : ''; ?>>
	                <?php echo ucfirst( esc_attr( $message_type ) ); ?>
	            </option>

        	<?php } ?>
        </select>
	    <?php
	}

	/**
	 * Message overwrite field callback function.
	 *
	 * @param array $args
	 */
	function enable_network_wide_message_cb( $args ) {
	    // Get the value of the setting we've registered with register_setting()
	    $options = get_option( 'asm_message' );
        ?>
	    <input
            type="checkbox"
            id="<?php echo esc_attr( $args['label_for'] ); ?>"
            name="asm_message[<?php echo esc_attr( $args['label_for'] ); ?>]"
	        <?php checked( true, $options[$args['label_for'] ] ); ?>

        />
        <i><?php esc_html_e ( 'Show a status message across all subsites.', 'admin-status-message' ); ?></i>
	    <?php
	}

	/**
	 * Output the text field for message
	 *
	 * @param      array  $args 
	 */
	function network_wide_message_cb( $args ) {
	    // Get the value of the setting we've registered with register_setting()
	    $options = get_option( 'asm_message' );
	    ?>
	    <input 
	    	type="text"
	    	id="<?php echo esc_attr( $args['label_for'] ); ?>"
	        name="asm_message[<?php echo esc_attr( $args['label_for'] ); ?>]"
	        value="<?php echo isset( $options[ $args['label_for'] ] ) ? esc_attr( $options[ $args['label_for'] ] ) : ''; ?>"
	    />
	    <?php
	}

	/**
	 * Sanitize fields from the form
	 *
	 * @param      array  $values  Array of values from the form
	 *
	 * @return     array  Sanitized array of values
	 */
	public function sanitize_fields( $values ) {
		$values['asm_message_field'] = sanitize_text_field( $values['asm_message_field'] );

		// Added the same filter as above so that the added fields can get sanitized automatically.
		$message_type_arr = apply_filters( 'message_type_arr', ['success', 'info', 'warning', 'error'] );
		$values['asm_message_type'] = in_array( $values['asm_message_type'], $message_type_arr ) ? $values['asm_message_type'] : '';

		if( is_multisite() && is_main_site() ) {
			$values['asm_enable_network_wide_message'] = isset( $values['asm_enable_network_wide_message'] ) ? true : false;
			
			$values['asm_network_wide_message_type'] = in_array( $values['asm_network_wide_message_type'], $message_type_arr ) ? $values['asm_network_wide_message_type'] : '';
			
			$values['asm_network_wide_message'] = sanitize_text_field( $values['asm_network_wide_message'] );
		}

		return $values;
	}

	public function load_scripts( $hook ) {
		if( 'tools_page_admin-status-message' === $hook ) { // load only on admin-status-message page
			wp_enqueue_script( 'asm-script', ASM_ROOT_URI_PATH . 'js/index.js', [], '1.0.0', false );
		}
	}

	/**
	 * Gets the array of message containg message and it's type.
	 *
	 * @return     array  Array containing the message and message type.
	 */
	static function get_message() {

		if( is_multisite() ) {
			switch_to_blog( get_main_site_id() );
			$options = get_option( 'asm_message' );
			
			if( isset( $options['asm_enable_network_wide_message'] ) && $options['asm_enable_network_wide_message'] ) {
				$message[ 'message_type' ] = isset( $options[ 'asm_message_type' ] ) ? $options[ 'asm_message_type' ] : '';
				$message['message'] = isset( $options[ 'asm_message_field' ] ) ? $options[ 'asm_message_field' ] : '';
				$message['network_wide_message'] = isset( $options[ 'asm_network_wide_message' ] ) ? $options[ 'asm_network_wide_message' ] : '';
				$message[ 'network_wide_message_type' ] = isset( $options[ 'asm_network_wide_message_type' ] ) ? $options[ 'asm_network_wide_message_type' ] : '';
				restore_current_blog(); // restore current blog when exiting this conditon
				
				// Get the display type of the current blog
				$options = get_option( 'asm_message' );
				$message['display_type'] = isset( $options[ 'asm_message_display_type' ] ) ? $options[ 'asm_message_display_type' ] : 'notice';
			} else {
				restore_current_blog(); // restore current blog if the above condition is not met

				$options = get_option( 'asm_message' );
				$message['message'] = isset( $options[ 'asm_message_field' ] ) ? $options[ 'asm_message_field' ] : '';
				$message[ 'message_type' ] = isset( $options[ 'asm_message_type' ] ) ? $options[ 'asm_message_type' ] : '';
				$message['display_type'] = isset( $options[ 'asm_message_display_type' ] ) ? $options[ 'asm_message_display_type' ] : 'notice';
			}

		} else {
			$options = get_option( 'asm_message' );
			$message['message'] = isset( $options[ 'asm_message_field' ] ) ? $options[ 'asm_message_field' ] : '';
			$message[ 'message_type' ] = isset( $options[ 'asm_message_type' ] ) ? $options[ 'asm_message_type' ] : '';
			$message['display_type'] = isset( $options[ 'asm_message_display_type' ] ) ? $options[ 'asm_message_display_type' ] : 'notice';
		}

		return apply_filters( 'asm_message', $message );
	}
}