<?php

namespace Sachyya\AdminStatusMessage\Admin;

use Sachyya\AdminStatusMessage\Admin\Setting;

/**
 * Class to display the message on dashboard
 */
class Notice {
	public static ?Notice $instance = null;


	/**
	 * Gets the instance.
	 *
	 * @return     Notice|null  The instance.
	 */
	public static function get_instance(): ?Notice {
		if ( is_null( self::$instance ) ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Notice constructor
	 */
	public function __construct() {
		if( is_multisite() ) { // have to check is_multisite since is_network_admin does not check for multisite
		 	if( is_network_admin() ) {
				add_action( 'network_admin_notices', [ $this, 'add_admin_notice' ] );
			} else {
				add_action( 'admin_notices', [ $this, 'add_admin_notice' ] );
			}
		} else {
			add_action( 'admin_notices', [ $this, 'add_admin_notice' ] );
		}
	}

	/**
	 * Adds admin notice.
	 */
	public function add_admin_notice() {
		$message = Setting::get_message();
		
		if( ! empty( $message['network_wide_message'] ) ) {
			printf( '<div class="notice notice-%1$s is-dismissible"><p><strong>%2$s</strong></p></div>', esc_attr( $message['network_wide_message_type'] ), esc_html( $message['network_wide_message'] ) );
		}

		if( ! empty( $message['message'] ) ) {
			printf( '<div class="notice notice-%1$s is-dismissible"><p><strong>%2$s</strong></p></div>', esc_attr( $message['message_type'] ), esc_html( $message['message'] ) );
		}
	}
}