<?php
/*
 * Plugin Name: Admin Status Message
 * Description: A simple plugin that will allow posting a status message on the WordPress admin dashboard page.
 * Plugin URI: #
 * Author: Sachyya
 * Author URI: https://sachyya.github.io/
 * Version: 1.0.1
 * Requires at least: 5.7
 * Requires PHP:      7.4
 * License: http://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
 * Text Domain: admin-status-message
*/

defined( 'ABSPATH' ) or die( 'Script Kiddies Go Away' );

if ( ! defined( 'ASM_VERSION' ) ) {
	define( 'ASM_VERSION', '1.0.0' );
}

if ( ! defined( 'ASM_FILE_PATH' ) ) {
	define( 'ASM_FILE_PATH', __FILE__ );
}
if ( ! defined( 'ASM_ROOT_DIR_PATH' ) ) {
	define( 'ASM_ROOT_DIR_PATH', DIRNAME( __FILE__ ) );
}

if ( ! defined( 'ASM_ROOT_URI_PATH' ) ) {
	define( 'ASM_ROOT_URI_PATH', plugin_dir_url( __FILE__ ) );
}

// Simple debugger function for self use.
if ( ! function_exists( 'print_pre' ) ) {
	function print_pre( $variable ) {
		echo '<pre>';
		var_dump( $variable );
		echo '</pre>';
	}
}

// Boot the plugin
require_once ASM_ROOT_DIR_PATH . '/includes/Bootstrap.php';