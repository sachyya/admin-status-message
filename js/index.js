jQuery(document).ready(function($){
    $('#asm_message_display_type').change(function() {      
        if ( this.value === 'notice' ) {        
            $('#asm_message_type').parents().eq(1).show();                    
        } else {
            $('#asm_message_type').parents().eq(1).hide();    
        }
    });

    $('#asm_enable_network_wide_message').change(function() {      
        if ( this.checked ) {        
            $('#asm_network_wide_message').parents().eq(1).show();                    
            $('#asm_network_wide_message_type').parents().eq(1).show();                    
        } else {
            $('#asm_network_wide_message').parents().eq(1).hide();    
            $('#asm_network_wide_message_type').parents().eq(1).hide();    
        }
    });
});