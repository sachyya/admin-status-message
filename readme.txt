=== Admin Status Message ===
Contributors: sachyya-sachet
Donate link: 
Tags: admin-status-message, message, admin, status-message
Requires at least: 5.6
Tested up to: 5.8
Stable tag: 1.0.1
Requires PHP: 7.4
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

A simple plugin that will allow posting a status message on the WordPress admin dashboard page.

== Description ==

A simple plugin that will allow posting a status message on the WordPress admin dashboard page. It works on multi site installation also with a setting to override the message.

Features:

1. Write admin status message promptly
1. Work on both single and multi site installation
1. Super admin can override the message


== Installation ==

1. Upload the plugin files to the `/wp-content/plugins/plugin-name` directory, or install the plugin through the WordPress plugins screen directly.
1. Activate the plugin through the 'Plugins' screen in WordPress
1. Use the Tools->Admin Status Message screen to configure the plugin


== Changelog ==

= 1.0.0 - Jan 17, 2020 =
* Initial release

= 1.0.1 - Jan 31, 2020 =
* Add option to configute network wide status message separately.
* Remove network wide overwrite option

